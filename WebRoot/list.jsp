<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'list.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

</head>
<body>
	<a href="add.jsp">添加</a>
	<form action="selectOne.action" method="post">
	<input type="text" name="student.id">
	<input type="submit" value="id查询">
	</form>
	<table border="1">
	<tr>
	    <td>学号</td>
		<td>姓名</td>
		<td>地址</td>
		<td>修改学生信息</td>
		<td>删除学生信息</td>
	</tr>
	<c:forEach items="${list}" var="student">
	<tr>
	    <td>${student.id}</td>
		<td>${student.name}</td>
		<td>${student.address}</td>
		<td><a href="update.jsp?id=${student.id}&name=${student.name}&address=${student.address}">修改</a></td>
		<td><a href="delete.action?student.id=${student.id}">删除</a></td>
	</tr>
	</c:forEach>
	</table>
</body>
</html>
