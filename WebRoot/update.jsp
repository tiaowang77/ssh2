<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'update.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <h1>修改学生信息页面</h1>
    <form action="update.action" method="post">
	学号:<input type="text" name="student.id" value="${param.id}" readonly="readonly"><!-- id设置为只读 -->
	姓名:<input type="text" name="student.name" value="${param.name}"><!-- 获取表单输入的值 -->
	地址:<input type="text" name="student.address" value="${param.address}">
	<input type="submit" value="修改">
	</form>
  </body>
</html>
