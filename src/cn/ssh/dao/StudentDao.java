package cn.ssh.dao;

import java.util.List;


import org.apache.commons.collections.Transformer;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;


import cn.ssh.domain.Student;

public class StudentDao {
	private SessionFactory sessionFactory;

	public void save(Student student) {

		System.out.println("Dao涓璼ave鏂规硶鎵ц浜嗐�銆傘�");
		this.sessionFactory.getCurrentSession().save(student);
		System.out.println("娣诲姞鎴愬姛");

	}

	public void delete(Student student) {

		System.out.println("Dao涓璬elete鏂规硶鎵ц浜嗐�銆傘�");
		this.sessionFactory.getCurrentSession().delete(student);
		System.out.println("鍒犻櫎鎴愬姛");

	}

	public void update(Student student) {

		System.out.println("Dao涓璾pdate鏂规硶鎵ц浜嗐�銆傘�");
		this.sessionFactory.getCurrentSession().update(student);
		System.out.println("鏇存柊鎴愬姛");

	}

	public List<Student> query() {
		String sql = "select * from student";
		SQLQuery sqlQuery = this.sessionFactory.getCurrentSession().createSQLQuery(sql);
		sqlQuery.addEntity(Student.class);
		return sqlQuery.list();
	}
	
	public Student selectOne(Integer id){
		Student student =(Student)this.sessionFactory.getCurrentSession().get(Student.class,id);
		return student;
	} 

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}